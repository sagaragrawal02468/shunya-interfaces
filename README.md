# Shunya Interfaces

![Badge 1](https://img.shields.io/badge/build-passing-green?style=for-the-badge)
![Badge 2](https://img.shields.io/badge/supported%20boards-12-blueviolet?style=for-the-badge)
![Badge 3](https://img.shields.io/badge/sensors%20integrated-20-blue?style=for-the-badge)

Shunya Interfaces is a GPIO access library written in C, with C++ bindings. 
Shunya Interfaces allows you to rapidly create and deploy IoT products with ease. 

The motivation for the project is to make it very easy to create and deploy 
cute & cool IoT devices for people regardless of their specialization.

## Table of Contents
+ [Supported Boards](#supported-boards)
+ [Feature List](#features)
+ [Documentation](#documentation)
+ [Examples](#examples)
+ [Installation](#Installation)
+ [API Reference](#api-reference)
+ [License](#License)

---

## Supported Boards

| **Board Name** | **Board** |
| ------ | ------ |
| RaspberryPi 4B |<img src="/assets/boards/05.jpeg" width="180">|
| RaspberryPi 3/3B+ | <img src="/assets/boards/10.jpg" width="180"> |
| Nvidia Jetson Nano | <img src="/assets/boards/01.jpg" width="180"> |
| Asus TinkerBoard | <img src="/assets/boards/03.png" width="180">  |
| NanoPi M4 | <img src="/assets/boards/02.jpg" width="180"> |
| Rock960  | <img src="/assets/boards/11.png" width="180"> |
| Hikey970 | <img src="/assets/boards/04.png" width="180"> |
| OrangePi R1 | <img src="/assets/boards/09.jpg" width="180"> |
| OrangePi 2G IOT | <img src="/assets/boards/07.jpg" width="180"> | 
| OrangePi Zero | <img src="/assets/boards/06.jpg" width="180"> |
| OrangePi i96 | <img src="/assets/boards/08.jpg" width="180"> |
| Raxda RockPi S| <img src="/assets/boards/12.png" width="180"> |


## Features 

+ **No learning Curve**  
        With API's similar to Arduino, we ensure that users have an easy time using Shunya Interfaces. 
+ **Supports Multiple boards**  
        Shunya Interfaces API's work with multiple boards, providing users with similar experience on different boards.  
+ Create **Portable code on supported platforms**  
        With Shunya Interfaces identifying the board at runtime, Users can use the same code on all supported boards.  
+ **Sensor Libraries**  
        Shunya Interfaces has API's for most commonly used sensors, allowing Users to interface sensors faster.    
+ **Supports standard IoT protocols**  
        Users can now gather data and put on the cloud without hassle.

---  

## Documentation 
Check out our [User Documentation](/user-docs/user-docs-main-page.md) for comprehensive documentation on Shunya Interfaces. 

## Examples
See [Code examples](/examples).

## Installation
Install and start making IOT projects, See [How to Install Shunya Interfaces](/user-docs/how-to-install-shunya-interfaces.md) document. 

## API Reference
Check out our [API Reference](/user-docs/shunya-interfaces-API.md) document. 

## Credits
This GPIO access library is inspired from WiringPi. Please checkout the original library [WiringPi][WiringPi].

[WiringPi]: http://wiringpi.com/

## License
Check out [license](LICENSE.md)
